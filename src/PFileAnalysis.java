import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.github.habernal.confusionmatrix.ConfusionMatrix;

public class PFileAnalysis
{
	Common common = new Common(); 
	TreeMap<String,String> valMap = new TreeMap<String,String>();
	ArrayList<String> valMapImages = new ArrayList<String>();
	ArrayList<String> valMapIds = new ArrayList<String>();
	ArrayList<ArrayList<Double>> predictions = new ArrayList<ArrayList<Double>>();
	TreeMap<String,ArrayList<Double>> cityPredictions = new TreeMap<String,ArrayList<Double>>();
	TreeMap<String,Integer> cityMispredictions = new TreeMap<String,Integer>();
	//String path = "/home/kerryn/git/GoogleStaticMaps/test/Data/Output/";		
	String outputPath = "./Output/";
	String valmapPath = "./Data/";
	
	//TreeMap<String,TreeMap<String,Double>> valMapUrlsAndProb = new TreeMap<String,TreeMap<String,Double>>();
	TreeMap<String,Double> validationPredictionResults = new TreeMap<String,Double>();
	ArrayList<Double> allProbabilitiesForCities = new ArrayList<Double>();
	
	int TOP_X_RESULTS = 10;
	
	public static void main(String[] args)
	{
		PFileAnalysis analysis = new PFileAnalysis();	
		
		
		if (args.length > 0) 
		{
			analysis.setOutputPath(args[0]);
			analysis.setValmapPath(args[1]);
			int topXResults = new Integer(args[2]).intValue();
			analysis.setTOP_X_RESULTS(topXResults);
		}	
		analysis.doAnalysis();
	}
	
	
	
	
	public void doAnalysis()
	{
		
		//predictions_val.txt.p  process_p_file.R  train_map.txt  val_map.txt
	    String file = "val_map.txt";
	    String predictionsFile = "predictions_val.txt.p";
	    
	    System.out.println(getOutputPath() + predictionsFile);
        System.out.println(getValmapPath() + file);
		//System.exit(1);
		
		readValMap(valmapPath, file);
		readPredictions(outputPath, predictionsFile);
		//matchPredictions();
		//printPredictions();
    	
	}
	
	
	public void readValMap(String path, String file)
	{

	    ArrayList<String> text = common.readLargerTextFileAlternateToArray(path + file);
	    //System.out.println(text);
	    
	    for (String oneLine : text)
	    {
	    	
	        //System.out.println(oneLine);
	    	int lastSpace = oneLine.lastIndexOf("	");
	    	String cityUrl = oneLine.substring(0,lastSpace);
	    	//System.out.println(cityUrl);
	    	
	    	
	    	String[] newString = oneLine.split("/");
	    	String cityAndMore = newString[newString.length-1];
	    	String[] cityAndMoreSStr = cityAndMore.split("_");
	    	String city = cityAndMoreSStr[0];
	    	
	    	String[] idAndMore = cityAndMore.split("\t");
	    	String id = idAndMore[idAndMore.length-1];
	    	
	    	
	    	//System.out.println(city + " " + id);
	    	valMap.put(id, city);
	    	valMapIds.add(id);
	    	
	    	valMapImages.add(cityUrl);
	    	
//	    	TreeMap<String,Double> cityAndProbs = valMapUrlsAndProb.get(city);
//	    	if (cityAndProbs == null)
//	    	{
//	    		cityAndProbs = new TreeMap<String,Double>();
//	    	}
//	    	
//	    	Double defaultValue = 0.0;
//	    	cityAndProbs.put(cityUrl,defaultValue);
	    	
//    		//also build a city url and prob file which will later be sorted to get top 10 most probable images
//	    	TreeMap<String,Double> cityAndProbs = valMapUrlsAndProb.get(city);
//	    	if (cityAndProbs == null)
//	    	{
//	    		cityAndProbs = new TreeMap<String,Double>();
//	    	}
//	    	//cityAndProbs.put(cityUrl,lineOfPredictions.get(idInt));
//	    	Double defaultValue = 0.0;
//	    	cityAndProbs.put(cityUrl, defaultValue);
//	    	valMapUrlsAndProb.put(city, cityAndProbs);
	    	
	    }
	    
	}
	
	final static Charset ENCODING = StandardCharsets.UTF_8;
	   public void readPredictions(String path, String file)
	    {
	    	ConfusionMatrix cm = new ConfusionMatrix();
	    	
	    	//ArrayList<String> text = common.readLargerTextFileAlternateToArray(path + file);
	    	
	    	
	    	
	    
		    	    

	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	int count = 0;
	    	
	    	Path path2 = Paths.get(path + file);
		    try (BufferedReader reader = Files.newBufferedReader(path2, ENCODING))
		    {
		      String oneLine = null;
		      while ((oneLine = reader.readLine()) != null) 
		      {
	    	
	    	//for (String oneLine : text)
		    //{
		    	//int lastSpace = oneLine.lastIndexOf("	");
		    	//String cityUrl = oneLine.substring(0,lastSpace);
	    		
	    		
	    		ArrayList<Double> lineOfPredictions = new ArrayList<Double>();
	    		String[] newString = oneLine.split(" ");
	    		for (String item : newString)
	    		{
	    			Double value = new Double(item).doubleValue();
	    			lineOfPredictions.add(value);
	    		}
	    		predictions.add(lineOfPredictions);
	    				
	    		String id = valMapIds.get(count);
	    		int idInt = new Integer(id).intValue();
	    		String city = valMap.get(id);
	    		//Collections.max(indlCity);
	    		//System.out.println(lineOfPredictions.toString());
	    		int maxItemNumber = getIndexOfMax(lineOfPredictions);
	    		
	    		//System.out.println("maxItemNumber=" + maxItemNumber + " " + lineOfPredictions.get(maxItemNumber) + " idInt=" + idInt);
	    		
	    		if (maxItemNumber == idInt)
	    		{
	    			//System.out.println("Match of max " + lineOfPredictions.get(maxItemNumber) + " " + city);
	    			cm.increaseValue(city, city, 1);
	    		}
	    		else
	    		{
	    			//System.out.println(valMap.toString());
	    			String mistakenCity = valMap.get(""+maxItemNumber);
	    			//System.out.println("Not match " + city + " closest match " + mistakenCity);
	    			cm.increaseValue(city, mistakenCity, 1);
	    			String cityMispredictionsKey = city + "~" + mistakenCity;
	    			Integer mistakeCount = cityMispredictions.get(cityMispredictionsKey);
	    			if (mistakeCount == null)
	    			{
	    				mistakeCount = new Integer(0);
	    			}
	    			mistakeCount ++;
	    			cityMispredictions.put(cityMispredictionsKey, mistakeCount);
	    		}

	    		validationPredictionResults.put(valMapImages.get(count), lineOfPredictions.get(idInt));
	    		allProbabilitiesForCities.add(lineOfPredictions.get(idInt));     
	    		
	    		
	    		
	    		lineOfPredictions = null;
	    		newString = null;
	    		id = null;
	    		city= null;
	    		
	    		
	    		count ++;
		    }
	    	
	    	
	    	
	    	
	    	
	    	
	    	
		      //}      
		    }
			catch (IOException e)
			{			
				e.printStackTrace();
			}    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	      //System.out.println(cm);
	    	//System.out.println(cm);
	    	cm.setNumberOfDecimalPlaces(2);
	    	//System.out.println(cm.printLabelPrecRecFm());
	    	//System.out.println(cm.getPrecisionForLabels());
	    	//System.out.println(cm.getRecallForLabels());
	    	//System.out.println(cm.printNiceResults());
	    	common.writeFile(cm.toString(), path + "generatedConfusionMatrix.csv");
	    	
	    	Set<String> cityMispredictionsKeys = cityMispredictions.keySet();
	    	StringBuffer sb = new StringBuffer("Source" + '\t' + "Target" + '\t' + "Weight" + '\n');
	    	for (String key : cityMispredictionsKeys)
	    	{
	    		String[] splitKey = key.split("~");
	    		String line = splitKey[0] + '\t' + splitKey[1] + '\t' + cityMispredictions.get(key) + '\n';
	    		//System.out.println(line);
	    		if (splitKey[0].equals(splitKey[1]) || cityMispredictions.get(key) < 2)
	    		{
	    			
	    		}
	    		else
	    		{
	    			sb.append(line);
	    		}		
	    	}
	    	common.writeFile(sb.toString(), path + "generatedConfusionMatrixWeights.csv");
	    	
	    	
	    	StringBuffer valPredSB = new StringBuffer("Image" + '\t' + "Probability" + '\n');
	    	Set<String> validationPredictionResultsKeys = validationPredictionResults.keySet();
	    	for (String key : validationPredictionResultsKeys)
	    	{
	    		valPredSB.append(key + '\t' + validationPredictionResults.get(key) + '\n'); 			
	    	}
	    	common.writeFile(valPredSB.toString(), path + "generatedImageProbabilities.csv");
	    	////
	    	
	    	
	    	
	    	StringBuffer valPredHtml = new StringBuffer();
	    	valPredHtml.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"><html><head>" + '\n' );
	    	valPredHtml.append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />" + '\n');
	    	valPredHtml.append("<title>Top "
	    			+ TOP_X_RESULTS
	    			+ " highest probability images for each city</title>" + '\n');
	    	valPredHtml.append("<LINK REL=\"SHORTCUT ICON\" HREF=\"../favicon.ico\" >" + '\n');
	    	valPredHtml.append("</head>" + '\n');
	    	valPredHtml.append("<body><center><b>Top "
	    			+ TOP_X_RESULTS
	    			+ " highest probability images for each city</b></center><p />" + '\n');
	    	
//	    	valPredHtml.append("<hr />" + '\n');
//	    	valPredHtml.append("<center><b>City A </b></center>" + '\n');
//	    	valPredHtml.append("<br><br><img src=\"../images/2017/2017_0312/_IMG_9626.JPG\" alt=\"\"><b> PROB</b>" + '\n');
//	    	valPredHtml.append("<hr />" + '\n');
//	    	valPredHtml.append("<center><b>City B  </b></center>" + '\n');
//	    	valPredHtml.append("<br><br><img src=\"../images/2017/2017_0313/_IMG_9759.JPG\" alt=\"\"><b> PROB</b>" + '\n');



	    	StringBuffer valPredTopSB = new StringBuffer("Image" + '\t' + "Probability" + '\n');
	    	Set<String> valMapSetKeys = valMap.keySet();
	    	
	    	ArrayList<String> urls = new ArrayList<String>();
	    	ArrayList<Double> probs = new ArrayList<Double>();
	    	int countUrls = 0;
	    	boolean nextCity = false;
	    	String previousCity = "";
	    	for (String cityUrl : valMapImages)
	    	{
	    		String[] newString = cityUrl.split("/");
		    	String cityAndMore = newString[newString.length-1];
		    	String[] cityAndMoreSStr = cityAndMore.split("_");
		    	String city = cityAndMoreSStr[0];
		    	//System.out.println(city);
		    	//System.out.println(valMapImages.get(countUrls));
		    	//System.out.println(allProbabilitiesForCities.get(countUrls));
		    	
		    	

//		    	valPredHtml.append("<br><br><img src=\"../images/2017/2017_0312/_IMG_9626.JPG\" alt=\"\"><b> PROB</b>" + '\n');
		    	
		    	if (city.equals(previousCity))
		    	{
		    		urls.add(cityUrl);
		    		probs.add(allProbabilitiesForCities.get(countUrls));
		    	}
		    	else
		    	{
		    		//System.out.println(urls.toString());
		    		//System.out.println(probs.toString());
		    		int[] indexes = getSortedArrayIndexes(probs);
		    		valPredTopSB.append( getTopImagesStr(indexes, urls, probs));
		    		valPredHtml.append( getTopImagesHtmlStr(indexes, urls, probs, previousCity));
		    		previousCity = city;
		    		urls = new ArrayList<String>();
		    		probs = new ArrayList<Double>();
		    	}
		    	
		    	if (countUrls == validationPredictionResults.size()-1)
		    	{
		    		//System.out.println(urls.toString());
		    		//System.out.println(probs.toString());
		    		int[] indexes = getSortedArrayIndexes(probs);	    		
		    		valPredTopSB.append( getTopImagesStr(indexes, urls, probs));
		    		valPredHtml.append( getTopImagesHtmlStr(indexes, urls, probs, previousCity));
		    	}
	    		countUrls++;
	    	}
	    	
	    	valPredHtml.append("</body></html>" + '\n');
	    	
	    	common.writeFile(valPredHtml.toString(), path + "generatedTopImageProbabilities.html");
	    	common.writeFile(valPredTopSB.toString(), path + "generatedTopImageProbabilities.csv");

	    }
	    

    public void readPredictions_old(String path, String file)
    {
    	ConfusionMatrix cm = new ConfusionMatrix();
    	
    	ArrayList<String> text = common.readLargerTextFileAlternateToArray(path + file);
    	int count = 0;
    	for (String oneLine : text)
	    {
	    	//int lastSpace = oneLine.lastIndexOf("	");
	    	//String cityUrl = oneLine.substring(0,lastSpace);
    		
    		
    		ArrayList<Double> lineOfPredictions = new ArrayList<Double>();
    		String[] newString = oneLine.split(" ");
    		for (String item : newString)
    		{
    			Double value = new Double(item).doubleValue();
    			lineOfPredictions.add(value);
    		}
    		predictions.add(lineOfPredictions);
    				
    		String id = valMapIds.get(count);
    		int idInt = new Integer(id).intValue();
    		String city = valMap.get(id);
    		//Collections.max(indlCity);
    		//System.out.println(lineOfPredictions.toString());
    		int maxItemNumber = getIndexOfMax(lineOfPredictions);
    		
    		//System.out.println("maxItemNumber=" + maxItemNumber + " " + lineOfPredictions.get(maxItemNumber) + " idInt=" + idInt);
    		
    		if (maxItemNumber == idInt)
    		{
    			//System.out.println("Match of max " + lineOfPredictions.get(maxItemNumber) + " " + city);
    			cm.increaseValue(city, city, 1);
    		}
    		else
    		{
    			//System.out.println(valMap.toString());
    			String mistakenCity = valMap.get(""+maxItemNumber);
    			//System.out.println("Not match " + city + " closest match " + mistakenCity);
    			cm.increaseValue(city, mistakenCity, 1);
    			String cityMispredictionsKey = city + "~" + mistakenCity;
    			Integer mistakeCount = cityMispredictions.get(cityMispredictionsKey);
    			if (mistakeCount == null)
    			{
    				mistakeCount = new Integer(0);
    			}
    			mistakeCount ++;
    			cityMispredictions.put(cityMispredictionsKey, mistakeCount);
    		}

    		validationPredictionResults.put(valMapImages.get(count), lineOfPredictions.get(idInt));
    		allProbabilitiesForCities.add(lineOfPredictions.get(idInt));          	
    		count ++;
	    }
      //System.out.println(cm);
    	//System.out.println(cm);
    	cm.setNumberOfDecimalPlaces(2);
    	//System.out.println(cm.printLabelPrecRecFm());
    	//System.out.println(cm.getPrecisionForLabels());
    	//System.out.println(cm.getRecallForLabels());
    	//System.out.println(cm.printNiceResults());
    	common.writeFile(cm.toString(), path + "generatedConfusionMatrix.csv");
    	
    	Set<String> cityMispredictionsKeys = cityMispredictions.keySet();
    	StringBuffer sb = new StringBuffer("Source" + '\t' + "Target" + '\t' + "Weight" + '\n');
    	for (String key : cityMispredictionsKeys)
    	{
    		String[] splitKey = key.split("~");
    		String line = splitKey[0] + '\t' + splitKey[1] + '\t' + cityMispredictions.get(key) + '\n';
    		//System.out.println(line);
    		if (splitKey[0].equals(splitKey[1]) || cityMispredictions.get(key) < 2)
    		{
    			
    		}
    		else
    		{
    			sb.append(line);
    		}		
    	}
    	common.writeFile(sb.toString(), path + "generatedConfusionMatrixWeights.csv");
    	
    	
    	StringBuffer valPredSB = new StringBuffer("Image" + '\t' + "Probability" + '\n');
    	Set<String> validationPredictionResultsKeys = validationPredictionResults.keySet();
    	for (String key : validationPredictionResultsKeys)
    	{
    		valPredSB.append(key + '\t' + validationPredictionResults.get(key) + '\n'); 			
    	}
    	common.writeFile(valPredSB.toString(), path + "generatedImageProbabilities.csv");
    	////
    	
    	
    	
    	StringBuffer valPredHtml = new StringBuffer();
    	valPredHtml.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"><html><head>" + '\n' );
    	valPredHtml.append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />" + '\n');
    	valPredHtml.append("<title>Top "
    			+ TOP_X_RESULTS
    			+ " highest probability images for each city</title>" + '\n');
    	valPredHtml.append("<LINK REL=\"SHORTCUT ICON\" HREF=\"../favicon.ico\" >" + '\n');
    	valPredHtml.append("</head>" + '\n');
    	valPredHtml.append("<body><center><b>Top "
    			+ TOP_X_RESULTS
    			+ " highest probability images for each city</b></center><p />" + '\n');
    	
//    	valPredHtml.append("<hr />" + '\n');
//    	valPredHtml.append("<center><b>City A </b></center>" + '\n');
//    	valPredHtml.append("<br><br><img src=\"../images/2017/2017_0312/_IMG_9626.JPG\" alt=\"\"><b> PROB</b>" + '\n');
//    	valPredHtml.append("<hr />" + '\n');
//    	valPredHtml.append("<center><b>City B  </b></center>" + '\n');
//    	valPredHtml.append("<br><br><img src=\"../images/2017/2017_0313/_IMG_9759.JPG\" alt=\"\"><b> PROB</b>" + '\n');



    	StringBuffer valPredTopSB = new StringBuffer("Image" + '\t' + "Probability" + '\n');
    	Set<String> valMapSetKeys = valMap.keySet();
    	
    	ArrayList<String> urls = new ArrayList<String>();
    	ArrayList<Double> probs = new ArrayList<Double>();
    	int countUrls = 0;
    	boolean nextCity = false;
    	String previousCity = "";
    	for (String cityUrl : valMapImages)
    	{
    		String[] newString = cityUrl.split("/");
	    	String cityAndMore = newString[newString.length-1];
	    	String[] cityAndMoreSStr = cityAndMore.split("_");
	    	String city = cityAndMoreSStr[0];
	    	//System.out.println(city);
	    	//System.out.println(valMapImages.get(countUrls));
	    	//System.out.println(allProbabilitiesForCities.get(countUrls));
	    	
	    	

//	    	valPredHtml.append("<br><br><img src=\"../images/2017/2017_0312/_IMG_9626.JPG\" alt=\"\"><b> PROB</b>" + '\n');
	    	
	    	if (city.equals(previousCity))
	    	{
	    		urls.add(cityUrl);
	    		probs.add(allProbabilitiesForCities.get(countUrls));
	    	}
	    	else
	    	{
	    		//System.out.println(urls.toString());
	    		//System.out.println(probs.toString());
	    		int[] indexes = getSortedArrayIndexes(probs);
	    		valPredTopSB.append( getTopImagesStr(indexes, urls, probs));
	    		valPredHtml.append( getTopImagesHtmlStr(indexes, urls, probs, previousCity));
	    		previousCity = city;
	    		urls = new ArrayList<String>();
	    		probs = new ArrayList<Double>();
	    	}
	    	
	    	if (countUrls == validationPredictionResults.size()-1)
	    	{
	    		//System.out.println(urls.toString());
	    		//System.out.println(probs.toString());
	    		int[] indexes = getSortedArrayIndexes(probs);	    		
	    		valPredTopSB.append( getTopImagesStr(indexes, urls, probs));
	    		valPredHtml.append( getTopImagesHtmlStr(indexes, urls, probs, previousCity));
	    	}
    		countUrls++;
    	}
    	
    	valPredHtml.append("</body></html>" + '\n');
    	
    	common.writeFile(valPredHtml.toString(), path + "generatedTopImageProbabilities.html");
    	common.writeFile(valPredTopSB.toString(), path + "generatedTopImageProbabilities.csv");

    }
    
    public String getTopImagesStr(int[] indexes, ArrayList<String> urls, ArrayList<Double> probs)
    {
    	StringBuffer sb = new StringBuffer();
    	int count = 0;
    	for (int i=indexes.length-1;i>=0;i--)
		{
    		if (count >= TOP_X_RESULTS)
    		{
    			continue;
    		}
			//System.out.println(probs.get(i));
			sb.append(urls.get(i) + '\t' + probs.get(i) + '\n'); 
			count ++;
		}
    	
    	return sb.toString();
    }
    
    public String getTopImagesHtmlStr(int[] indexes, ArrayList<String> urls, ArrayList<Double> probs, String city)
    {
    	StringBuffer sb = new StringBuffer();
    	sb.append("<hr />" + '\n');
    	sb.append("<center><b>" + city + " </b></center>" + '\n');
    	int count = 0;
    	for (int i=indexes.length-1;i>=0;i--)
		{
    		if (count >= TOP_X_RESULTS)
    		{
    			continue;
    		}
			//System.out.println(probs.get(i));
    		
    		sb.append(" <img src=\""
    				+ urls.get(i)
    				+ "\" alt=\"\"><b> "
    				+ probs.get(i)
    				+ "</b>" + '\n');
    		
			//sb.append(urls.get(i) + '\t' + probs.get(i) + '\n'); 
			count ++;
		}
    	
    	return sb.toString();
    } 
    
    

    public int[] getSortedArrayIndexes(ArrayList<Double> unsortedArray)
    {
		ArrayList<Double> nstore = new ArrayList<Double>(unsortedArray); 
		Collections.sort(unsortedArray);
		int[] indexes = new int[unsortedArray.size()];
		//for (int n = unsortedArray.size()-1; n >=0 ; n--)		
		for (int n = unsortedArray.size()-1; n >=0 ; n--)	
		{
		    indexes[n] = nstore.indexOf(unsortedArray.get(n));
		}
		//System.out.println(Arrays.toString(indexes));
		return indexes;
    }
    
    public int[] reverseArray(int inputArray[])   
    {
        int temp;
        for (int i = 0; i < inputArray.length/2; i++) 
        {
            temp = inputArray[i];
            inputArray[i] = inputArray[inputArray.length-1-i];
            inputArray[inputArray.length-1-i] = temp;
        }
        return inputArray;
    }
         
    
    public ArrayList<Double> getArraySortedByIndex(int[] indexes, ArrayList<Double> unsortedArray)
    {
    	ArrayList<Double> nstore = new ArrayList<Double>(unsortedArray); 
		//Collections.sort(unsortedArray);
		//int[] indexes = new int[unsortedArray.size()];
    	//for (int n = 0; n < unsortedArray.size(); n++)
    	for (int n = unsortedArray.size()-1; n >=0 ; n--)	
		{
			unsortedArray.set(indexes[n], nstore.get(indexes[n])) ;
		}
		//System.out.println(Arrays.toString(indexes));
		return unsortedArray;
    }
    
    public int getIndexOfMax(ArrayList<Double> arr)
    {
        double MaxVal = arr.get(0); // take first as MaxVal
        int indexOfMax = 0; //returns -1 if all elements are equal
        for (int i = 0; i < arr.size(); i++) 
        {
            //if current is less then MaxVal
            if(arr.get(i) > MaxVal )
            {
                MaxVal = arr.get(i); // put it in MaxVal
                indexOfMax = i; // put index of current Max
            }
        }
        return indexOfMax;  
    }
    
    public void matchPredictions()
    {
    	int count = 0;
    	for (ArrayList<Double> lineOfPredictions : predictions)
    	{
    		String id = valMapIds.get(count);
    		int idInt = new Integer(id).intValue();
    		String city = valMap.get(id);
    		Double predictedValue = lineOfPredictions.get(idInt);
    		//System.out.println(count + " " + id + " " + city + " " + predictedValue);
    		
    		ArrayList<Double> indlCity = cityPredictions.get(id);
    		if (indlCity == null)
    		{
    			indlCity = new ArrayList<Double>();
    		}
    		indlCity.add(predictedValue);
    		cityPredictions.put(id, indlCity);
    		

    	}
    	
    }
	public String getOutputPath()
	{
		return outputPath;
	}
	public void setOutputPath(String outputPath)
	{
		this.outputPath = outputPath;
	}
	public String getValmapPath()
	{
		return valmapPath;
	}
	public void setValmapPath(String valmapPath)
	{
		this.valmapPath = valmapPath;
	}
	public <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) 
	{
	    Comparator<K> valueComparator =  new Comparator<K>() 
	    {
	        public int compare(K k1, K k2) 
	        {
	            int compare = map.get(k2).compareTo(map.get(k1));
	            if (compare == 0) return 1;
	            else return compare;
	        }
	    };
	    Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
	    sortedByValues.putAll(map);
	    return sortedByValues;
	}




	public int getTOP_X_RESULTS()
	{
		return TOP_X_RESULTS;
	}




	public void setTOP_X_RESULTS(int tOP_X_RESULTS)
	{
		TOP_X_RESULTS = tOP_X_RESULTS;
	}
 

}

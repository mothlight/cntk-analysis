import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Common
{
	public void writeFile(String text, String filename)
	{
		FileOutputStream out; // declare a file output object
		PrintStream p; // declare a print stream object

		try
		{
			out = new FileOutputStream(filename);
			p = new PrintStream(out);
			p.println(text);
			p.close();
		} catch (Exception e)
		{
			System.err.println("Error writing to file");
		}

	}
	final static Charset ENCODING = StandardCharsets.UTF_8;
	
	   public ArrayList<String> readLargerTextFileAlternateToArray(String aFileName) 
	   {	   
		   ArrayList<String> returnValue = new ArrayList<String>();
		    Path path = Paths.get(aFileName);
		    try (BufferedReader reader = Files.newBufferedReader(path, ENCODING))
		    {
		      String line = null;
		      while ((line = reader.readLine()) != null) 
		      {
		    	  returnValue.add(line);
		      }      
		    }
			catch (IOException e)
			{			
				e.printStackTrace();
			}
		    return returnValue;
		  } 
	   
		static void writeFile(String path, Charset encoding, String str) throws IOException 
		{
	
			
			PrintWriter out = new PrintWriter(path);
			out.println(str);
			out.close();

			
		}
}
